﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallColor : MonoBehaviour {

    private Renderer WallMat;
    private Destroywall wallhealth;
    public Material damaged;
	// Use this for initialization

	void Start () {
        WallMat = GetComponent<Renderer>();
        wallhealth = GetComponentInChildren<Destroywall>();
    }
	
	// Update is called once per frame
	void Update () {
        if (wallhealth.wallhealth < 30)
        {
            Debug.Log("Red");
            WallMat.material = damaged;
        }
    }
}
