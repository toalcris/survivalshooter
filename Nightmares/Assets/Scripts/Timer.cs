﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
    public float timer = 60;

    // Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            timer = 60;
        }
    }
    void OnGui()
    {
        GUI.Label(new Rect(100, 100, 200, 100), "More Enemies in:" + (int)timer);
    }
}
