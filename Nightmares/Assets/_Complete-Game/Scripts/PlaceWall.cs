﻿using CompleteProject;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceWall : MonoBehaviour {

    public GameObject wall;
    public GameObject slant;
    public GameObject gun;
    public int speed;
    private bool wallactive = true;
    private bool buildmode = false;
    public GameObject costinfo;
    //public GameObject brickprefab;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate () {

        if (Input.GetKey(KeyCode.UpArrow))
            wall.transform.Rotate(Vector3.right * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.DownArrow))
            wall.transform.Rotate(-Vector3.right * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow))
            wall.transform.Rotate(Vector3.up * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow))
            wall.transform.Rotate(-Vector3.up * speed * Time.deltaTime);

        if (Input.GetButton("1"))
        {
            gun.SetActive(true);
            wall.SetActive(false);
            slant.SetActive(false);
            buildmode = false;
            costinfo.SetActive(false);
            Debug.Log(wallactive);
        }
        if (Input.GetButton("2"))
        {
            wall.SetActive(true);
            slant.SetActive(false);
            gun.SetActive(false);
            costinfo.SetActive(true);
            wallactive = true;
            buildmode = true;
            Debug.Log(wallactive);
        }
        if (Input.GetButton("3"))
        {
            wall.SetActive(false);
            slant.SetActive(true);
            gun.SetActive(false);
            costinfo.SetActive(true);
            wallactive = false;
            buildmode = true;
            Debug.Log(wallactive);
        }

        if (Input.GetButtonDown("Fire2"))
            {
            if (buildmode == true)
            {
                if (ScoreManager.score >= 20)
                {
                    if (wallactive == true)
                    {
                        Instantiate(wall, wall.transform.position, wall.transform.rotation);
                        ScoreManager.score -= 20;
                        Debug.Log(ScoreManager.score);
                    }
                    if (wallactive == false)
                    {
                        Instantiate(slant, slant.transform.position, slant.transform.rotation);
                        ScoreManager.score -= 20;
                        Debug.Log(ScoreManager.score);
                    }
                }
            }   
        }
	}
}
