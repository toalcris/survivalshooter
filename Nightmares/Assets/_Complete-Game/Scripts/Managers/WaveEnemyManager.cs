﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



    [System.Serializable]
    public class WaveAction
    {
        public string name;
        public float delay;
        public Transform prefab;
        public int spawnCount;
        public string message;

        
}

    [System.Serializable]
    public class Wave
    {
        public string name;
        public List<WaveAction> actions;

}


public class WaveEnemyManager : MonoBehaviour
    {
        public float difficultyFactor = 0.9f;
        public List<Wave> waves;
        private Wave m_CurrentWave;
        public Wave CurrentWave { get { return m_CurrentWave; } }
        private float m_DelayFactor = 1.0f;
        public Transform[] spawnPoints;
    public static int numEnemies = 0;


    IEnumerator SpawnLoop()
        {
            m_DelayFactor = 1.0f;
          // while (true)
           // {
                foreach (Wave W in waves)
                {
                    m_CurrentWave = W;
                    foreach (WaveAction A in W.actions)
                    {
                        if (A.delay > 0)
                            yield return new WaitForSeconds(A.delay * m_DelayFactor);
                        if (A.message != "")
                        {
                            // TODO: print ingame message
                        }
                        if (A.prefab != null && A.spawnCount > 0)
                        {
                            for (int i = 0; i < A.spawnCount; i++)
                            {
                            // Find a random index between zero and one less than the number of spawn points.
                            int spawnPointIndex = Random.Range(0, spawnPoints.Length);
                        WaveEnemyManager.numEnemies++;
                        //Debug.Log("enemies =" + WaveEnemyManager.numEnemies);
                            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
                            Instantiate(A.prefab, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
                            //Instantiate(enemy);
                            }
                        }
                        }
                    }
                    yield return null;  // prevents crash if all delays are 0
                }
                //m_DelayFactor *= difficultyFactor;
                //yield return null;  // prevents crash if all delays are 0
          // }
    void Start()
    {
        StartCoroutine(SpawnLoop());
    }
}