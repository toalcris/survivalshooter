﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CompleteProject
{
    public class TimeManager : MonoBehaviour
    {
        public float timer = 0;
        public GameObject spawn;
        public GameObject spawn2;
        public GameObject spawn3;
        public GameObject spawn4;
        public GameObject spawn5;
        public GameObject spawn6;
        public GameObject spawn7;
        public GameObject spawn8;
        public GameObject spawn9;
        public GameObject spawn10;

        public static int level = 1;


        Text text;

        void Awake ()
        {
            // Set up the reference.
            text = GetComponent <Text> ();
        }


        void Update ()
        {
            timer -= Time.deltaTime;
            //Debug.Log(level);

            if (timer <= 0)
            {
                timer = 60;
                spawn.SetActive(false);
                spawn2.SetActive(false);
                spawn3.SetActive(false);
                spawn4.SetActive(false);
                spawn5.SetActive(false);
                spawn6.SetActive(false);
                spawn7.SetActive(false);
                spawn8.SetActive(false);
                spawn9.SetActive(false);
                spawn10.SetActive(false);

                level++;
            }
            if (timer <= 30 && timer >=25)
            {
                spawn.SetActive(false);
                spawn2.SetActive(false);
                spawn3.SetActive(false);
                spawn4.SetActive(false);
                spawn5.SetActive(false);
                spawn6.SetActive(false);
                spawn7.SetActive(false);
                spawn8.SetActive(false);
                spawn9.SetActive(false);
                spawn10.SetActive(false);
            }
            if (timer <10)
            {
                text.color = Color.red;
            }
            else
            {
                text.color = Color.white;
            }
            if (timer <= 1 && timer >= 0)
            {
                if (level == 1)
                {
                    Debug.Log("test");
                    spawn2.SetActive(true);
                    //StartCoroutine(addlevel());
                }
                else if (level == 2)
                {
                    Debug.Log("test2");
                    spawn3.SetActive(true);
                    //StartCoroutine(addlevel());
                    //level++;
                }
                else if (level == 3)
                {
                    Debug.Log("test3");
                    spawn4.SetActive(true);
                    //StartCoroutine(addlevel());
                    //level++;
                }
                else if (level == 4)
                {
                    Debug.Log("test");
                    spawn5.SetActive(true);
                    //StartCoroutine(addlevel());
                }
                else if (level == 5)
                {
                    Debug.Log("test2");
                    spawn6.SetActive(true);
                    //StartCoroutine(addlevel());
                    //level++;
                }
                else if (level == 6)
                {
                    Debug.Log("test3");
                    spawn7.SetActive(true);
                    //StartCoroutine(addlevel());
                    //level++;
                }
                else if (level == 7)
                {
                    Debug.Log("test");
                    spawn8.SetActive(true);
                    //StartCoroutine(addlevel());
                }
                else if (level == 8)
                {
                    Debug.Log("test2");
                    spawn9.SetActive(true);
                    //StartCoroutine(addlevel());
                    //level++;
                }
                else if (level == 9)
                {
                    Debug.Log("test3");
                    spawn10.SetActive(true);
                    //StartCoroutine(addlevel());
                    //level++;
                }
            }
            /*
            if (timer > 50 && timer < 55)
            {
                spawn.SetActive(false);
                spawn2.SetActive(false);
                spawn3.SetActive(false);
            }
            */
            // Set the displayed text to be the word "Score" followed by the score value.
            if (level > 10)
            {
                text.color = Color.red;
                text.text = "You Win";

            }
            else
            {
                text.text = "More Enemies In: " + (int)timer;
            }
        }
        /*
        IEnumerator addlevel()
        {
            spawn.SetActive(false);
            spawn2.SetActive(false);
            spawn3.SetActive(false);
            Debug.Log("test4");
            yield return new WaitForSeconds(10);
            Debug.Log("test5");
            level++;


        }
        */
    }
}