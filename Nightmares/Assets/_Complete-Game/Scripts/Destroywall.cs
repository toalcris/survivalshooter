﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroywall : MonoBehaviour {

    public int wallhealth = 100;
    bool enemyInRange;
    public float timeBetweenAttacks = 0.5f;     // The time in seconds between each attack.
    float timer;
    public GameObject wall;
    //private Renderer WallMat;
    


    void OnTriggerEnter(Collider other)
    {
        // If the entering collider is the player...
        if (other.gameObject.tag == "enemy")
        {
            // ... the player is in range.
            enemyInRange = true;
            //Debug.Log("enter");
        }
    }

    void OnTriggerExit(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject.tag == "enemy")
        {
            // ... the player is no longer in range.
            enemyInRange = false;
            //Debug.Log("exit");
        }
    }

    // Use this for initialization
    void Start () {
        //WallMat = wall.GetComponent<Renderer>();
    }
	
	// Update is called once per frame
	void Update () {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && enemyInRange && wallhealth > 0)
        {
            // ... attack.
            Attack();
        }
        if (wallhealth == 0)
        {
            Debug.Log("destroy");
            Destroy(transform.parent.gameObject);
        }
        /*
        if (wallhealth < 30)
        {
            WallMat.material.color = Color.red;
        }
        */
    }

    void Attack()
    {
        // Reset the timer.
        timer = 0f;
        //Debug.Log("attack");
        // If the player has health to lose...
        if (wallhealth > 0)
        {
            Debug.Log(wallhealth);
            // ... damage the player.
            wallhealth -= 5;
        }
    }

}
